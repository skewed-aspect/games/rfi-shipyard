# RFI Shipyard

This is a ship uploading/modification/flying simulator application. It's intended as a tech demo for various game 
development technologies.

## Concept

Thanks largely in part to Matt Hoadley, we have a _lot_ of ships models that would be fun to try flying around and 
playing with. So, instead of trying to guess at how to make them relate, the idea was to create a interactive way to 
upload these ships, and modify their stats until they _feel_ right. This also makes for a great way to view a ship, 
or compare several ships next to each other.

### Features

These are the things I'd like to see:

* [ ] Ability to upload new ships
* [ ] Individual ship viewer
* [ ] Ability to add/modify/save stats for a ship
* [ ] Ability to 'Test fly' the ship with most recently stats
* [ ] Ship 'painter'

Not sure how many of these will get implemented, but I'd like to see these features at a minimum.
