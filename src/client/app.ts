// ---------------------------------------------------------------------------------------------------------------------
// Main App
// ---------------------------------------------------------------------------------------------------------------------

import { createApp } from 'vue';
import { createRouter, createWebHistory } from 'vue-router';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// Components
import AppComponent from './app.vue';

// Pages
import MainPage from './pages/main.vue';
import AboutPage from './pages/about.vue';

// Site Style
import './scss/theme.scss';

// Site Icons
import * as icons from './lib/icons';

// ---------------------------------------------------------------------------------------------------------------------
// Vue Router
// ---------------------------------------------------------------------------------------------------------------------

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', name: 'home', component: MainPage },
        { path: '/about', name: 'about', component: AboutPage },

        // Fallback to home
        { path: '/:catchAll(.*)', redirect: { name: 'home' } }
    ]
})

// ---------------------------------------------------------------------------------------------------------------------
// Vue App
// ---------------------------------------------------------------------------------------------------------------------

const app = createApp(AppComponent);

// Add icons to the library
library.add(icons);

// Create the font awesome component
app.component('FaIcon', FontAwesomeIcon);

// Use the router we configured
app.use(router);

// ---------------------------------------------------------------------------------------------------------------------

// Initialization
async function init() : Promise<void>
{
    console.log('RFI Shipyard initialized.');
}

// ---------------------------------------------------------------------------------------------------------------------

init()
    .then(() => app.mount('#main-app'))
    .catch((err) =>
    {
        console.error('Failed to initialize application:', err.stack);
    });

// ---------------------------------------------------------------------------------------------------------------------
