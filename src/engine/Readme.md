# Skewed Aspect Game Engine (SAGE)

Sage is a higher level game engine wrapping around [BabylonJS][]. The main goal of SAGE is to wrap up as much of the 
boilerplate in getting started as possible. It's a full game engine, with its own entity management and behavior system.

## Documentation

For more information, please see the [docs][] folder.

[BabylonJS]: https://www.babylonjs.com/
[docs]: docs/index.md
